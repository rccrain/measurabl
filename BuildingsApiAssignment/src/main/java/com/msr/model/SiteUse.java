package com.msr.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "site_use")
public class SiteUse {
	@Id
	private int id;

	private int siteId;

	private String description;

	private long sizeSqft;

	private int useTypeId;

	@ManyToOne(targetEntity = Site.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "site")
	@JsonBackReference(value = "site-Ref")
	private Site site;

}
