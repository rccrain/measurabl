package com.msr.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name="site")
public class Site {
	@Id
	private int id;

	private String name;

	private String address;

	private String city;

	private String state;

	private String zipcode;
	
	@OneToMany(targetEntity=SiteUse.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "site")
	@JsonManagedReference(value="site-Ref") 
	private List<SiteUse> siteUses;
	
	@Transient
	private UseType primaryType;
	
	@Transient
	private Long totalSize;

}
