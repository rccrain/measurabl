package com.msr.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@JsonInclude(Include.NON_NULL)
public class SiteDto {
	
	private int id;
	
	private String name;

	private String address;

	private String city;

	private String state;

	private String zipcode;
	
	//private List<SiteUseDto> siteUses;
	
	private UseTypeDto primaryType;
	
	private Long totalSize;
}
