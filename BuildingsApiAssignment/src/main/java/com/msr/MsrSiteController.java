package com.msr;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.msr.model.SiteDto;
import com.msr.service.SiteServiceFacade;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/mrsSite")
public class MsrSiteController {

	@Autowired 
	SiteServiceFacade siteServiceFacade;	

	@ApiOperation("Returns all Sites in the H2 Database.")
	@GetMapping("getAllSites")
	public List<SiteDto> getAllSites() {
		return siteServiceFacade.getAllSites();
	}
	
	@ApiOperation("Returns a Site by Id with its primary UseType and total square footage.")
	@GetMapping("getPrimarySiteById")
	public SiteDto getPrimarySiteById(Integer id) {
		try {
			return siteServiceFacade.getPrimarySiteById(id);
		} catch(NoResultException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Could Not Find Site for Id: " + id, e);
		} catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unkown exception searching for Site for Id: " + id, e);
		}
		
	}
	
	@ApiOperation("Returns all Sites by state. Abbreviation or full state name can be passed.")
	@GetMapping("getSitesByState")
	public List<SiteDto> getSitesByState(String state) {
		try {
			return siteServiceFacade.getSitesByState(state);
		} catch(NoResultException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Could Not Find Site for State: " + state, e);
		} catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unkown exception searching for Site by state: " + state, e);
		}
	}
	
	@ApiOperation("Delete a Site.")
	@DeleteMapping("deleteSite")
	public void createSite(Integer id) {
		try {
			siteServiceFacade.deleteSite(id);
		} catch(EmptyResultDataAccessException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Could Not Find Site to delete for Id: " + id, e);
		} catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unkown exception deleting Site for Id: " + id, e);
		}
	}
}
