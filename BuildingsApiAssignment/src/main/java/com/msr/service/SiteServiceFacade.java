package com.msr.service;

import java.util.List;

import com.msr.model.Site;
import com.msr.model.SiteDto;
import com.msr.model.SiteUse;
import com.msr.model.UseType;

public interface SiteServiceFacade {
	
	public List<SiteDto> getAllSites();
	
	public Site getSiteById(Integer id);
	
	public SiteDto getPrimarySiteById(Integer id);
	
	public List<SiteDto> getSitesByState(String state);
	
	public void createSite(SiteDto site);
	
	public void deleteSite(Integer id);
	
	public void saveSites(List<Site> sites);
	
	public void saveSiteUses(List<SiteUse> siteUses);
	
	public void saveUseTypes(List<UseType> useTypes);
}
