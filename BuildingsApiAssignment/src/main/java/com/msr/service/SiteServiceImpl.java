package com.msr.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.msr.data.SiteRepository;
import com.msr.data.SiteUseRepository;
import com.msr.data.State;
import com.msr.data.UseTypeRepository;
import com.msr.model.Site;
import com.msr.model.SiteDto;
import com.msr.model.SiteUse;
import com.msr.model.UseType;

@Service
public class SiteServiceImpl implements SiteServiceFacade {
	@Autowired
	private SiteRepository siteRepo;
	
	@Autowired
	private SiteUseRepository siteUseRepo;
	
	@Autowired
	private UseTypeRepository useTypeRepo;

	@Autowired
	private ModelMapper modelMapper;
	
	@PersistenceContext
	private Session session;
	
	public List<SiteDto> getAllSites() {
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Site> cr = cb.createQuery(Site.class);
		Root<Site> root = cr.from(Site.class);
		cr.select(root);
	
		Query<Site> query = session.createQuery(cr);
		List<Site> results = query.getResultList();
		List<SiteDto> siteDtos = new ArrayList<SiteDto>();

		for(Site site : results) {
			siteDtos.add(this.convertToDto(site));
		}
		return siteDtos;
	}
	
	public SiteDto getPrimarySiteById(Integer id) {
		Site site = this.getSiteById(id);
		site.setTotalSize(new Long(0));
		
		Map<Integer, Long> primarySiteMap = new HashMap<Integer, Long>();
		
		for(SiteUse siteUse: site.getSiteUses()) {
			site.setTotalSize(site.getTotalSize() + siteUse.getSizeSqft());
			primarySiteMap.compute(siteUse.getUseTypeId(), (k,v) -> (v == null) ? siteUse.getSizeSqft() : v + siteUse.getSizeSqft());
		}
		
		Long totalSqFt = new Long(0);
		Integer primaryTypeId = new Integer(0);
		for(Map.Entry<Integer, Long> entry : primarySiteMap.entrySet()) {
			if(totalSqFt < entry.getValue()) {
				totalSqFt = entry.getValue();
				primaryTypeId = entry.getKey();
			}
				
		}
		
		UseType useType = this.getUseTypeById(primaryTypeId);
		site.setPrimaryType(useType);
		//how to handle if there are equivalents?
		return this.convertToDto(site);
	}
	
	public List<SiteDto> getSitesByState(String state) {
		List<Site> sites = new ArrayList<Site>();
		State st;
		if(state.length() > 2) {
			 st = State.valueOfName(state); 
		} else if(state.length() == 2) {
			st = State.valueOfAbbreviation(state);
		} else {
			st = State.UNKNOWN;
		}
		
		if(st != State.UNKNOWN) {
			sites = this.getSiteByState(st.getAbbreviation());
		} else {
			throw new NoResultException("State not found for state: " + state);
		}
		List<SiteDto> siteDtos = new ArrayList<SiteDto>();

		for(Site site : sites) {
			siteDtos.add(this.convertToDto(site));
		}
		return siteDtos;
	}
	
	public Site getSiteById(Integer id) {
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Site> cr = cb.createQuery(Site.class);
		Root<Site> root = cr.from(Site.class);
		cr.select(root).where(root.get("id").in(id));
	
		Query<Site> query = session.createQuery(cr);
		return query.getSingleResult();
	}
	
	public UseType getUseTypeById(Integer id) {
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<UseType> cr = cb.createQuery(UseType.class);
		Root<UseType> root = cr.from(UseType.class);
		cr.select(root).where(root.get("id").in(id));
	
		Query<UseType> query = session.createQuery(cr);
		return query.getSingleResult();
	}
	
	public void saveSiteUses(List<SiteUse> siteUses) {
		siteUseRepo.saveAll(siteUses);
	}

	public void saveUseTypes(List<UseType> useTypes) {
		useTypeRepo.saveAll(useTypes);
	}
	
	public void saveSites(List<Site> sites) {
		siteRepo.saveAll(sites);
	}
	
	public void createSite(SiteDto site) {
		Site s = this.convertToEntity(site);
		siteRepo.save(s);
	}
	
	public void deleteSite(Integer id) {
		siteRepo.deleteById(id);
	}
	
	private List<Site> getSiteByState(String abbrv) {
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Site> cr = cb.createQuery(Site.class);
		Root<Site> root = cr.from(Site.class);
		cr.select(root).where(root.get("state").in(abbrv));
	
		Query<Site> query = session.createQuery(cr);
		return query.getResultList();
	}
	
	private SiteDto convertToDto(Site site) {
		SiteDto siteDto = new SiteDto();
			
		siteDto = modelMapper.map(site, SiteDto.class);
		return siteDto;
	}
	
	/*
	 * private SiteUseDto convertToDto(SiteUse siteUse) { SiteUseDto siteUseDto =
	 * modelMapper.map(siteUse, SiteUseDto.class); return siteUseDto; }
	 */
	private Site convertToEntity(SiteDto siteDto) {
		Site site = modelMapper.map(siteDto, Site.class);
		
		return site;
	}

}
