package com.msr;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.msr.model.Site;
import com.msr.model.SiteUse;
import com.msr.model.UseType;
import com.msr.service.SiteServiceFacade;

@SpringBootApplication
public class BuildingsApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(BuildingsApiApplication.class, args);
	}
	
	@Bean
	CommandLineRunner runner(SiteServiceFacade siteServiceFacade) {
		return args -> {
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Site>> siteTypeReference = new TypeReference<List<Site>>(){};
			InputStream input = TypeReference.class.getResourceAsStream("/data/sites.json");
			List<Site> sites = mapper.readValue(input, siteTypeReference);
			Map<Integer, Site> siteHash = new HashMap<Integer, Site>();
			siteHash = sites.stream().collect(Collectors.toMap(s -> s.getId(), s -> s));
			input.close();
			
			TypeReference<List<UseType>> useTypeTypeReference = new TypeReference<List<UseType>>(){};
			input = TypeReference.class.getResourceAsStream("/data/use_types.json");
			try {
				List<UseType> useTypes = mapper.readValue(input, useTypeTypeReference);
				siteServiceFacade.saveUseTypes(useTypes);
				System.out.println("UseType table built");
			} catch(IOException e) {
				System.out.println("Failed to build UseType table: " + e.getMessage());
			}
			input.close();
			
			TypeReference<List<SiteUse>> siteUseTypeReference = new TypeReference<List<SiteUse>>(){};
			input = TypeReference.class.getResourceAsStream("/data/site_uses.json");
			try {
				List<SiteUse> siteUses = mapper.readValue(input, siteUseTypeReference);
				for(SiteUse siteUse : siteUses) {
					siteUse.setSite(siteHash.get(siteUse.getSiteId()));
				}
				siteServiceFacade.saveSiteUses(siteUses);
				System.out.println("Site and SiteUse table built");
			} catch(IOException e) {
				System.out.println("Failed to build SiteUse table: " + e.getMessage());
			}
			input.close();
			
			
		};
	}
	
	
}
