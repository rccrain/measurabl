package com.msr.data;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.msr.model.Site;

/**
 * A sample JPA repository for querying and storing sites
 */
@Repository
public interface SiteRepository extends PagingAndSortingRepository<Site, Integer> {
	
}