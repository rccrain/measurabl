package com.msr.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.msr.model.UseType;

/**
 * A sample JPA repository for use types
 */
@Repository
public interface UseTypeRepository extends JpaRepository<UseType, Integer> {
}
