package com.msr.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.msr.model.SiteUse;

/**
 * A sample JPA repository for site use
 */
@Repository
public interface SiteUseRepository extends JpaRepository<SiteUse, Integer> {
}
